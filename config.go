package loj

import (
	"io/ioutil"

	"bitbucket.org/kusabashira/loj/utils"
	"bitbucket.org/kusabashira/loj/utils/path"
)

type Config struct {
	Current string
}

func LoadConfig() (config *Config, err error) {
	src, err := ioutil.ReadFile(path.ConfigFile())
	if err != nil {
		return nil, err
	}

	err = utils.Unmarshal(src, &config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func (c *Config) Store() error {
	dst, err := utils.Marshal(&c)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path.ConfigFile(), dst, 0644)
}
