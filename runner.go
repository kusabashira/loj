package loj

import (
	"bytes"
	"io/ioutil"
	"os/exec"
	"text/template"

	"bitbucket.org/kusabashira/loj/utils"
	"github.com/kusabashira/go-bytesstorage"
)

type Runner struct {
	Pre  []string
	Exe  []string
	Post []string
}

func executeTemplate(src []byte, vars map[string]string) ([]byte, error) {
	tmpl, err := template.New("runner").Parse(string(src))
	if err != nil {
		return nil, err
	}

	w := bs.NewBytesStorage()
	if err = tmpl.Execute(w, vars); err != nil {
		return nil, err
	}
	return w.Load(), nil
}

func LoadRunner(runner_path string, vars map[string]string) (runner *Runner, err error) {
	src, err := ioutil.ReadFile(runner_path)
	if err != nil {
		return nil, err
	}

	src, err = executeTemplate(src, vars)
	if err != nil {
		return nil, err
	}
	if err = utils.Unmarshal(src, &runner); err != nil {
		return nil, err
	}
	return runner, nil
}

func (r *Runner) judgeEachCase(p *Problem) (*Result, error) {
	result := &Result{}
	for _, c := range p.Case {
		cmd := exec.Command(r.Exe[0], r.Exe[1:]...)
		cmd.Stdin = bytes.NewReader([]byte(c.Input))

		expect := []byte(c.Output)
		actual, err := cmd.Output()
		switch {
		case err != nil:
			result.Append(false, err)
		case !bytes.Equal(expect, actual):
			result.Append(false, err)
		default:
			result.Append(true, nil)
		}
	}
	return result, nil
}

func (r *Runner) Judge(p *Problem) (*Result, error) {
	defer r.postProcess()
	if err := r.preProcess(); err != nil {
		return nil, err
	}
	return r.judgeEachCase(p)
}

func (r *Runner) preProcess() error {
	if len(r.Pre) < 1 {
		return nil
	}
	return exec.Command(r.Pre[0], r.Pre[1:]...).Run()
}

func (r *Runner) postProcess() error {
	if len(r.Post) < 1 {
		return nil
	}
	return exec.Command(r.Post[0], r.Post[1:]...).Run()
}
