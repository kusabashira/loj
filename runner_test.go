package loj_test

import (
	"reflect"
	"testing"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
)

func TestLoadRunner(t *testing.T) {
	expect := &loj.Runner{
		Pre:  []string{"gcc", "solve.c"},
		Exe:  []string{"a.out"},
		Post: []string{"rm", "a.out"},
	}

	path.SwitchAssetDir("asset")
	actual, err := loj.LoadRunner(
		path.RunnerFile("c"),
		map[string]string{
			"file": "solve.c",
		},
	)
	if err != nil {
		t.Fatal("Failed load:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("\nExpected %v\nbut %v\n", expect, actual)
	}
}
