package loj

type Result struct {
	Passes []bool
	Errors []error
}

func (r *Result) Append(pass bool, err error) {
	r.Passes = append(r.Passes, pass)
	switch {
	case err != nil:
		r.Errors = append(r.Errors, err)
	default:
		r.Errors = append(r.Errors, nil)
	}
}

func (r *Result) NoCase() int {
	return len(r.Passes)
}

func (r *Result) NoFailures() int {
	n := 0
	for _, pass := range r.Passes {
		if !pass {
			n += 1
		}
	}
	return n
}

func (r *Result) AllPassed() bool {
	for _, pass := range r.Passes {
		if !pass {
			return false
		}
	}
	return true
}
