package main

import (
	"fmt"
	"os"

	"github.com/gonuts/commander"
)

const (
	COMMAND_NAME = "loj"
	VERSION      = "0.2.0"
)

var command = &commander.Command{
	UsageLine: COMMAND_NAME,
	Short:     "cli interface for local online judge",
	Subcommands: []*commander.Command{
		cmd_checkout,
		cmd_info,
		cmd_setup,
		cmd_status,
		cmd_submit,
		cmd_version,
	},
}

func main() {
	err := command.Dispatch(os.Args[1:])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", COMMAND_NAME, err)
		os.Exit(1)
	}
}
