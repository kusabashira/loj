package main

import (
	"fmt"

	"github.com/gonuts/commander"
)

var cmd_version = &commander.Command{
	UsageLine: "version",
	Short:     "print loj version",
	Run: func(cmd *commander.Command, args []string) error {
		fmt.Println(VERSION)
		return nil
	},
}
