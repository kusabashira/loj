package main

import (
	"fmt"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
	"github.com/gonuts/commander"
	"github.com/gonuts/flag"
)

func init() {
	cmd_submit.Flag.String("t", "", "problem name")
}

var cmd_submit = &commander.Command{
	UsageLine: "submit FILE",
	Short:     "submit solve program",
	Flag:      *flag.NewFlagSet("submit", flag.ExitOnError),
	Run: func(cmd *commander.Command, args []string) error {
		cmd.Flag.Parse(args)
		if cmd.Flag.NArg() < 1 {
			return fmt.Errorf("no input solve program")
		}
		solver_name := cmd.Flag.Arg(0)
		if !path.Exist(solver_name) {
			return fmt.Errorf("no such as file %v", solver_name)
		}

		config, err := loj.LoadConfig()
		if err != nil {
			return err
		}

		problem_name := cmd.Flag.Lookup("t").Value.Get().(string)
		if problem_name == "" {
			if config.Current == "" {
				return fmt.Errorf("no specify problem with '-t'")
			}
			problem_name = config.Current
		}

		problem, err := loj.LoadProblem(path.ProblemFile(problem_name))
		if err != nil {
			return err
		}

		runner, err := loj.LoadRunner(
			path.RunnerFile(path.Ext(solver_name)),
			map[string]string{"file": solver_name})
		if err != nil {
			return err
		}

		result, err := runner.Judge(problem)
		if err != nil {
			return err
		}
		for _, pass := range result.Passes {
			if pass {
				fmt.Print(".")
			} else {
				fmt.Print("F")
			}
		}
		fmt.Printf("\n%d cases, %d failures\n",
			result.NoCase(),
			result.NoFailures())
		if result.AllPassed() {
			fmt.Println("All case passed.")
		}
		return nil
	},
}
