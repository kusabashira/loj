package main

import (
	"fmt"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
	"github.com/gonuts/commander"
)

var cmd_info = &commander.Command{
	UsageLine: "info NAME",
	Short:     "print infomation of problem",
	Run: func(cmd *commander.Command, args []string) error {
		if len(args) < 1 {
			cmd.Usage()
			return nil
		}
		problem_name := args[0]

		info, err := loj.LoadProblem(path.ProblemFile(problem_name))
		if err != nil {
			return err
		}

		fmt.Println(info.Name)
		fmt.Print(info.Description)
		return nil
	},
}
