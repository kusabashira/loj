package main

import (
	"fmt"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
	"github.com/gonuts/commander"
)

var cmd_checkout = &commander.Command{
	UsageLine: "checkout NAME",
	Short:     "change current problem",
	Run: func(cmd *commander.Command, args []string) error {
		cmd.Flag.Parse(args)
		if len(args) < 1 {
			cmd.Usage()
			return nil
		}
		problem_name := cmd.Flag.Arg(0)
		if !path.Exist(path.ProblemFile(problem_name)) {
			return fmt.Errorf("no such as problem %v", problem_name)
		}

		config, err := loj.LoadConfig()
		if err != nil {
			return err
		}
		config.Current = problem_name

		return config.Store()
	},
}
