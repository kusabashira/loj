package main

import (
	"bitbucket.org/kusabashira/loj/utils/path"
	"github.com/gonuts/commander"
)

var cmd_setup = &commander.Command{
	UsageLine: "setup",
	Short:     "setup directory structure",
	Run: func(cmd *commander.Command, args []string) error {
		return path.SetupAssetDir()
	},
}
