package main

import (
	"bitbucket.org/kusabashira/loj"
	"fmt"

	"github.com/gonuts/commander"
)

var cmd_status = &commander.Command{
	UsageLine: "status",
	Short:     "show the working status",
	Run: func(cmd *commander.Command, args []string) error {
		config, err := loj.LoadConfig()
		if err != nil {
			return err
		}

		if config.Current == "" {
			config.Current = "[empty]"
		}
		fmt.Println("*", config.Current)
		return nil
	},
}
