package utils_test

import (
	"reflect"
	"testing"

	"bitbucket.org/kusabashira/loj/utils"
)

type TestType struct {
	A string
	B string
}

func TestMarshal(t *testing.T) {
	src := &TestType{
		A: "abc",
		B: "def",
	}
	expect := []byte(`
A = "abc"
B = "def"
`[1:])
	actual, err := utils.Marshal(&src)
	if err != nil {
		t.Fatalf("Failed marshal %v:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("Expected %v\nbut %v\n", expect, actual)
	}
}

func TestUnmarshal(t *testing.T) {
	src := []byte(`
A = "ghi"
B = "jkl"
`[1:])
	expect := &TestType{
		A: "ghi",
		B: "jkl",
	}
	actual := &TestType{}
	if err := utils.Unmarshal(src, &actual); err != nil {
		t.Fatalf("Failed marshal %v:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("Expected %v\nbut %v\n", expect, actual)
	}
}
