package path

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/kusabashira/loj/utils/bindata"
	"github.com/mitchellh/go-homedir"
)

const (
	asset_dir_name     = ".loj"
	config_file_name = "config"
	runner_dir_name  = "runner"
	problem_dir_name = "problem"
	config_extension = ".tml"
)

var asset_dir string

func Ext(path string) string {
	ext := filepath.Ext(path)
	return strings.Replace(ext, ".", "", 1)
}

func Exist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func AssetDir() string {
	return filepath.Join(asset_dir)
}

func ConfigFile() string {
	return filepath.Join(asset_dir, config_file_name+config_extension)
}

func RunnerFile(runner_name string) string {
	return filepath.Join(asset_dir, runner_dir_name, runner_name+config_extension)
}

func ProblemFile(problem_name string) string {
	return filepath.Join(asset_dir, problem_dir_name, problem_name+config_extension)
}

func SetupAssetDir() error {
	if !Exist(asset_dir) {
		if err := os.Mkdir(asset_dir, 0755); err != nil {
			return err
		}
	}
	for _, asset := range bindata.AssetNames() {
		if err := bindata.RestoreAssets(asset_dir, asset); err != nil {
			return err
		}
	}
	return nil
}

func SwitchAssetDir(path string) {
	asset_dir = path
}

func init() {
	home, err := homedir.Dir()
	if err != nil {
		fmt.Fprintln(os.Stderr, "loj:", err)
		os.Exit(1)
	}
	SwitchAssetDir(filepath.Join(home, asset_dir_name))
}
