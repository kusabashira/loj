package path_test

import (
	"path/filepath"
	"testing"

	"bitbucket.org/kusabashira/loj/utils/path"
)

func TestConfigFile(t *testing.T) {
	path.SwitchAssetDir("test")
	if path.ConfigFile() != filepath.Join("test", "config.tml") {
		t.Error("config path broken")
	}
}

func TestRunnerFile(t *testing.T) {
	path.SwitchAssetDir("test")
	if path.RunnerFile("c") != filepath.Join("test", "runner", "c.tml") {
		t.Error("runner path broken")
	}
}

func TestProblemFile(t *testing.T) {
	if path.ProblemFile("hello_world") != filepath.Join("test", "problem", "hello_world.tml") {
		t.Error("problem path broken")
	}
}
