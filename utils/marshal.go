package utils

import (
	"github.com/BurntSushi/toml"
	"github.com/kusabashira/go-bytesstorage"
)

func Marshal(v interface{}) ([]byte, error) {
	w := bs.NewBytesStorage()
	if err := toml.NewEncoder(w).Encode(v); err != nil {
		return nil, err
	}
	return w.Load(), nil
}

func Unmarshal(data []byte, v interface{}) error {
	return toml.Unmarshal(data, v)
}
