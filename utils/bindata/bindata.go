package bindata

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
	"os"
	"time"
	"io/ioutil"
	"path"
	"path/filepath"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindata_file_info struct {
	name string
	size int64
	mode os.FileMode
	modTime time.Time
}

func (fi bindata_file_info) Name() string {
	return fi.name
}
func (fi bindata_file_info) Size() int64 {
	return fi.size
}
func (fi bindata_file_info) Mode() os.FileMode {
	return fi.mode
}
func (fi bindata_file_info) ModTime() time.Time {
	return fi.modTime
}
func (fi bindata_file_info) IsDir() bool {
	return false
}
func (fi bindata_file_info) Sys() interface{} {
	return nil
}

var _config_tml = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x72\x2e\x2d\x2a\x4a\xcd\x2b\x51\xb0\x55\x50\x52\xe2\x02\x04\x00\x00\xff\xff\x41\x9a\x1b\xae\x0d\x00\x00\x00")

func config_tml_bytes() ([]byte, error) {
	return bindata_read(
		_config_tml,
		"config.tml",
	)
}

func config_tml() (*asset, error) {
	bytes, err := config_tml_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "config.tml", size: 13, mode: os.FileMode(436), modTime: time.Unix(1425898473, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

var _problem_hello_world_tml = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\xca\x4b\xcc\x4d\x55\xb0\x55\x50\xf2\x48\xcd\xc9\xc9\x57\x08\xcf\x2f\xca\x49\x51\xe2\x4a\x49\x2d\x4e\x2e\xca\x2c\x28\xc9\xcc\xcf\x03\xc9\x29\x29\x71\x15\x14\x65\xe6\x95\xa0\xa9\x02\x89\x73\x45\x47\x27\x27\x16\xa7\xc6\xc6\x72\x65\xe6\x15\x94\x96\x40\x55\x83\x70\x7e\x69\x09\x42\x00\x49\x1f\x58\x12\x10\x00\x00\xff\xff\xda\x29\xd6\x37\x76\x00\x00\x00")

func problem_hello_world_tml_bytes() ([]byte, error) {
	return bindata_read(
		_problem_hello_world_tml,
		"problem/hello_world.tml",
	)
}

func problem_hello_world_tml() (*asset, error) {
	bytes, err := problem_hello_world_tml_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "problem/hello_world.tml", size: 118, mode: os.FileMode(436), modTime: time.Unix(1425892778, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

var _problem_x_cubic_tml = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\xca\x4b\xcc\x4d\x55\xb0\x55\x50\x8a\x50\x70\x2e\x4d\xca\x4c\x56\xe2\x4a\x49\x2d\x4e\x2e\xca\x2c\x28\xc9\xcc\xcf\x03\x89\x2b\x29\x71\x25\x27\xe6\x24\x97\xe6\x24\x96\xa4\x2a\x54\xc4\x19\x73\x81\x44\xb8\xa2\xa3\x93\x13\x8b\x53\x63\x63\xb9\x32\xf3\x0a\x4a\x4b\xa0\xea\x8c\xc0\x72\xf9\xa5\x25\x08\x21\x0b\x3c\xca\x0d\x30\x95\x1b\xe0\x51\xae\x6b\x8a\xa9\x5e\xd7\xd0\x08\x22\x0a\x08\x00\x00\xff\xff\xae\x56\xfd\x75\xc6\x00\x00\x00")

func problem_x_cubic_tml_bytes() ([]byte, error) {
	return bindata_read(
		_problem_x_cubic_tml,
		"problem/x_cubic.tml",
	)
}

func problem_x_cubic_tml() (*asset, error) {
	bytes, err := problem_x_cubic_tml_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "problem/x_cubic.tml", size: 198, mode: os.FileMode(436), modTime: time.Unix(1425892778, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

var _runner_c_tml = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x2a\x28\x4a\x55\x50\xb0\x55\x88\x56\x4a\x4f\x4e\x56\xd2\x51\x50\xaa\xae\xd6\x4b\xcb\xcc\x49\xad\xad\x55\x8a\xe5\x4a\xad\x80\xca\x25\xea\xe5\x97\x96\x00\x05\x0a\xf2\x8b\x4b\xc0\x02\x45\xb9\x20\xb5\x30\x61\x40\x00\x00\x00\xff\xff\x7e\xea\x8c\x76\x44\x00\x00\x00")

func runner_c_tml_bytes() ([]byte, error) {
	return bindata_read(
		_runner_c_tml,
		"runner/c.tml",
	)
}

func runner_c_tml() (*asset, error) {
	bytes, err := runner_c_tml_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "runner/c.tml", size: 68, mode: os.FileMode(436), modTime: time.Unix(1425892778, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

var _runner_lua_tml = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x4a\xad\x48\x55\xb0\x55\x88\x56\xca\x29\x4d\x54\xd2\x51\x50\xaa\xae\xd6\x4b\xcb\xcc\x49\xad\xad\x55\x8a\xe5\x02\x04\x00\x00\xff\xff\x68\xfd\xf1\x45\x1b\x00\x00\x00")

func runner_lua_tml_bytes() ([]byte, error) {
	return bindata_read(
		_runner_lua_tml,
		"runner/lua.tml",
	)
}

func runner_lua_tml() (*asset, error) {
	bytes, err := runner_lua_tml_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "runner/lua.tml", size: 27, mode: os.FileMode(436), modTime: time.Unix(1425892778, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"config.tml": config_tml,
	"problem/hello_world.tml": problem_hello_world_tml,
	"problem/x_cubic.tml": problem_x_cubic_tml,
	"runner/c.tml": runner_c_tml,
	"runner/lua.tml": runner_lua_tml,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for name := range node.Children {
		rv = append(rv, name)
	}
	return rv, nil
}

type _bintree_t struct {
	Func func() (*asset, error)
	Children map[string]*_bintree_t
}
var _bintree = &_bintree_t{nil, map[string]*_bintree_t{
	"config.tml": &_bintree_t{config_tml, map[string]*_bintree_t{
	}},
	"problem": &_bintree_t{nil, map[string]*_bintree_t{
		"hello_world.tml": &_bintree_t{problem_hello_world_tml, map[string]*_bintree_t{
		}},
		"x_cubic.tml": &_bintree_t{problem_x_cubic_tml, map[string]*_bintree_t{
		}},
	}},
	"runner": &_bintree_t{nil, map[string]*_bintree_t{
		"c.tml": &_bintree_t{runner_c_tml, map[string]*_bintree_t{
		}},
		"lua.tml": &_bintree_t{runner_lua_tml, map[string]*_bintree_t{
		}},
	}},
}}

// Restore an asset under the given directory
func RestoreAsset(dir, name string) error {
        data, err := Asset(name)
        if err != nil {
                return err
        }
        info, err := AssetInfo(name)
        if err != nil {
                return err
        }
        err = os.MkdirAll(_filePath(dir, path.Dir(name)), os.FileMode(0755))
        if err != nil {
                return err
        }
        err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
        if err != nil {
                return err
        }
        err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
        if err != nil {
                return err
        }
        return nil
}

// Restore assets under the given directory recursively
func RestoreAssets(dir, name string) error {
        children, err := AssetDir(name)
        if err != nil { // File
                return RestoreAsset(dir, name)
        } else { // Dir
                for _, child := range children {
                        err = RestoreAssets(dir, path.Join(name, child))
                        if err != nil {
                                return err
                        }
                }
        }
        return nil
}

func _filePath(dir, name string) string {
        cannonicalName := strings.Replace(name, "\\", "/", -1)
        return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

