package loj_test

import (
	"reflect"
	"testing"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
)

func TestConfig(t *testing.T) {
	path.SwitchAssetDir("asset")
	expect := &loj.Config{
		Current: "x_cubic",
	}
	if err := expect.Store(); err != nil {
		t.Fatalf("Failed store %v:", err)
	}

	actual, err := loj.LoadConfig()
	if err != nil {
		t.Fatalf("Failed load %v:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("Expected %v\n but %v\n", expect, actual)
	}
}

func TestConfigEmpty(t *testing.T) {
	path.SwitchAssetDir("asset")
	expect := &loj.Config{
		Current: "",
	}
	if err := expect.Store(); err != nil {
		t.Fatalf("Failed store %v:", err)
	}

	actual, err := loj.LoadConfig()
	if err != nil {
		t.Fatalf("Failed load %v:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("Expected %v\n but %v\n", expect, actual)
	}
}
