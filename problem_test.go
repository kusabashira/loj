package loj_test

import (
	"reflect"
	"testing"

	"bitbucket.org/kusabashira/loj"
	"bitbucket.org/kusabashira/loj/utils/path"
)

func TestLoadProblem(t *testing.T) {
	expect := &loj.Problem{
		Name:        "Hello World",
		Description: `print "Hello World"` + "\n",
		Case: []loj.Case{
			loj.Case{
				Input:  ``,
				Output: `Hello World` + "\n",
			},
		},
	}

	path.SwitchAssetDir("asset")
	actual, err := loj.LoadProblem(path.ProblemFile("hello_world"))
	if err != nil {
		t.Fatal("Failed load:", err)
	}
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("Expected %v\n but %v\n", expect, actual)
	}
}
