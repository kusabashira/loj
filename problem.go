package loj

import (
	"io/ioutil"

	"bitbucket.org/kusabashira/loj/utils"
)

type Case struct {
	Input  string
	Output string
}

type Problem struct {
	Name        string
	Description string
	Case        []Case
}

func LoadProblem(problem_path string) (problem *Problem, err error) {
	src, err := ioutil.ReadFile(problem_path)
	if err != nil {
		return nil, err
	}

	err = utils.Unmarshal(src, &problem)
	if err != nil {
		return nil, err
	}

	return problem, nil
}
